#!/usr/bin/env python3
import threading
from copy import deepcopy

import numpy as np
import plotly.graph_objs as go
import rospy
from control_msgs.msg import FollowJointTrajectoryActionGoal
from dash import Dash, Input, Output, dcc, html


class DataStore:
    """
    Store data, share data. Thread-safe.
    """

    def __init__(self):
        self._lock = threading.Lock()
        self._timestamps = None
        self._velocities = None
        self._new_data = False
        self._current_figure = None

    def update(self, timestamps: np.array, velocities: np.array):
        """
        Cache current ts, velocities
        """
        with self._lock:
            self._timestamps = timestamps
            self._velocities = velocities
            self._new_data = True

    def get_data(self) -> tuple:
        """
        Return data

        """
        with self._lock:
            return (
                deepcopy(self._timestamps),
                deepcopy(self._velocities),
                self._new_data,
            )

    def get_figure(self):
        with self._lock:
            return deepcopy(self._current_figure)

    def set_figure(self, figure):
        with self._lock:
            self._current_figure = figure

    def acknowledge(self):
        with self._lock:
            self._new_data = False


# Initialize data store
data_store = DataStore()

# Initialize Dash app with a more detailed layout
app = Dash(__name__)
app.layout = html.Div(
    [
        html.H1(
            "Trajectory Velocity Visualization",
            style={"textAlign": "center", "color": "#2c3e50"},
        ),
        html.Div(
            [
                dcc.Graph(id="velocity-plot"),
                html.Div(id="update-info", style={"textAlign": "center"}),
                dcc.Interval(id="interval-component", interval=1000, n_intervals=0),
            ],
            style={"margin": "20px"},
        ),
    ]
)


def create_figure(timestamps, velocities):
    """Helper function to create a figure from data."""
    traces = [
        go.Scatter(
            x=timestamps,
            y=velocities[:, i],
            mode="lines+markers",
            name=f"Velocity {name}",
            line={"width": 2},
            marker={"size": 4},
        )
        for i, name in enumerate(["X", "Y", "Yaw", "Z"])
    ]

    layout = go.Layout(
        title={"text": "Velocity Components Over Time", "font": {"size": 24}},
        xaxis={"title": "Time (s)", "gridcolor": "lightgray", "showgrid": True},
        yaxis={
            "title": "Velocity (rad/s or m/s)",
            "gridcolor": "lightgray",
            "showgrid": True,
        },
        showlegend=True,
        legend={
            "orientation": "h",
            "yanchor": "bottom",
            "y": 1.02,
            "xanchor": "right",
            "x": 1,
        },
        plot_bgcolor="white",
        paper_bgcolor="white",
        margin={"l": 60, "r": 30, "t": 50, "b": 50},
    )

    return {"data": traces, "layout": layout}


@app.callback(
    [Output("velocity-plot", "figure"), Output("update-info", "children")],
    Input("interval-component", "n_intervals"),
)
def update_plot(n):
    """Callback to update the plot with the latest data."""
    timestamps, velocities, new_data = data_store.get_data()

    if timestamps is None or velocities is None:
        # Return empty figure if no data has ever been received
        initial_figure = {
            "data": [],
            "layout": go.Layout(
                title="Waiting for data...",
                xaxis={"title": "Time (s)"},
                yaxis={"title": "Velocity (rad/s or m/s)"},
            ),
        }
        return initial_figure, "No data received yet"

    if new_data:
        figure = create_figure(timestamps, velocities)
        data_store.set_figure(figure)
        data_store.acknowledge()
        return figure, "Plot updated with new data"
    else:
        # Return the stored figure
        current_figure = data_store.get_figure()
        if current_figure is None:
            current_figure = create_figure(timestamps, velocities)
            data_store.set_figure(current_figure)
        return current_figure, "Displaying last received data"


def moveit_data_from_msg(msg):
    """Extracts timestamps and velocities from the FollowJointTrajectoryActionGoal message."""
    cmd_timestamps = [pt.time_from_start.to_sec() for pt in msg.goal.trajectory.points]
    cmd_vels = [pt.velocities for pt in msg.goal.trajectory.points]
    return np.array(cmd_timestamps), np.array(cmd_vels)


def callback(msg):
    """Callback function for the ROS subscriber."""
    rospy.loginfo("Received a new trajectory goal message!")
    timestamps, velocities = moveit_data_from_msg(msg)
    data_store.update(timestamps, velocities)


def run_dash():
    """Runs the Dash app."""
    app.run_server(debug=False, port=8050, host="0.0.0.0")


if __name__ == "__main__":
    try:
        # Initialize ROS node
        rospy.init_node("trajectory_velocity_plotter", anonymous=True)

        rospy.Subscriber(
            "/testbed_velocity_controller/follow_joint_trajectory/goal",
            FollowJointTrajectoryActionGoal,
            callback,
        )

        rospy.loginfo("Node initialized. Waiting for messages...")

        # Start Dash in a separate thread
        dash_thread = threading.Thread(target=run_dash)
        dash_thread.daemon = True
        dash_thread.start()

        # Keep the ROS node running
        rospy.spin()

    except rospy.ROSInterruptException:
        pass
