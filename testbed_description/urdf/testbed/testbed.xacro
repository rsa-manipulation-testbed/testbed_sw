<?xml version="1.0"?>
<robot name="testbed" xmlns:xacro="http://www.ros.org/wiki/xacro">

    <xacro:property name="joint_damping" value="10.0" />
    <xacro:property name="joint_friction" value="0.0" />

    <!-- Constants -->
    <xacro:property name="PI" value="3.1415926535897931"/>

    <!-- Frame Dimensions
         Outer dimensions from CAD for collision
    -->
    <xacro:property name="tank_diameter" value="2.0"/>

    <xacro:property name="y_length" value="1.833"/>
    <xacro:property name="y_width" value="0.610"/>
    <xacro:property name="y_height" value="0.102"/>

    <xacro:property name="x_length" value="0.508"/>
    <xacro:property name="x_width" value="0.3556"/>
    <xacro:property name="x_height" value="0.303"/>

    <xacro:property name="yaw_length" value="1.011"/>
    <xacro:property name="yaw_radius" value="0.189"/>

    <xacro:property name="z_length" value="0.147"/>
    <xacro:property name="z_radius" value="0.189"/>

    <!-- Y Offsets from CAD
    MIN and MAX from CAD - need to factor in origin offsets in final limit
    -->
    <xacro:property name="y_orig_x" value="-0.00175"/>
    <xacro:property name="y_orig_y" value="0.0806207"/>
    <xacro:property name="y_min" value="${0.33-y_orig_y}"/>
    <xacro:property name="y_max" value="${1.615-y_orig_y}"/>

    <!-- X Offsets from CAD -->
    <xacro:property name="x_orig_x" value="0.16712"/>
    <xacro:property name="x_max" value="${1.573-x_orig_x}"/>

    <!-- Yaw Offsets from CAD -->
    <xacro:property name="yaw_orig_yaw" value="${-PI/2}"/>
    <xacro:property name="yaw_min" value="0.035"/>
    <xacro:property name="yaw_max" value="6.257"/>

    <!-- Z Offsets from CAD -->
    <xacro:property name="z_orig_z" value="0.060"/>
    <xacro:property name="z_min" value="0.060"/>
    <xacro:property name="z_max" value="${0.698-z_orig_z}"/>

    <!-- LINKS -->
    <link name="functional_home">
    </link>

    <link name="z_mounting_origin">
    </link>

    <!-- Testbed Frame -->
    <link name="testbed_frame">
        <visual>
            <origin xyz="0 0 0" rpy="0 0 0"/>
            <geometry>
                <mesh filename="package://testbed_description/meshes/testbed/testbed_frame.dae"/>
            </geometry>
        </visual>
        <collision>
            <origin xyz="0 0 0" rpy="0 0 0"/>
            <geometry>
                <mesh filename="package://testbed_description/meshes/testbed/testbed_frame_collision.dae"/>
            </geometry>
        </collision>
        <inertial>
            <mass value="27"/>
            <inertia ixx="30" ixy="0.0" ixz="0.0" iyy="18" iyz="0.0" izz="11.5"/>
        </inertial>
    </link>

    <!-- Y Stage -->
    <link name="y_axis_link">
        <visual>
            <origin xyz="0 0 0" rpy="0 0 0"/>
            <geometry>
                <mesh filename="package://testbed_description/meshes/testbed/y_axis.dae"/>
            </geometry>
        </visual>
        <collision>
            <origin xyz="0 0 0" rpy="0 0 0"/>
            <geometry>
                <mesh filename="package://testbed_description/meshes/testbed/y_axis_collision.dae"/>
            </geometry>
        </collision>
        <inertial>
            <mass value="24"/>
            <inertia ixx="8" ixy="0.0" ixz="0.0" iyy="9" iyz="0.0" izz="1"/>
        </inertial>
    </link>

    <!-- X Stage -->
    <link name="x_axis_link">
        <visual>
            <origin xyz="0 0 0" rpy="0 0 0"/>
            <geometry>
                <mesh filename="package://testbed_description/meshes/testbed/x_axis.dae"/>
            </geometry>
        </visual>
        <collision>
            <origin xyz="0 0 -0.30725" rpy="0 0 0"/>
            <geometry>
                <box size="${x_width} ${x_length} ${x_height}"/>
            </geometry>
        </collision>
        <inertial>
            <mass value="4.5"/>
            <inertia ixx="0.05" ixy="0.0" ixz="0.0" iyy="0.10" iyz="0.0" izz="0.15"/>
        </inertial>
    </link>

    <!-- Yaw Stage -->
    <link name="yaw_axis_link">
        <visual>
            <origin xyz="0 0 0" rpy="0 0 0"/>
            <geometry>
                <mesh filename="package://testbed_description/meshes/testbed/yaw_axis.dae"/>
            </geometry>
        </visual>
        <collision>
            <origin xyz="0 0 0" rpy="0 0 ${PI/6}"/>
            <geometry>
                <mesh filename="package://testbed_description/meshes/testbed/yaw_axis_collision.dae"/>
            </geometry>
        </collision>
        <inertial>
            <mass value="13.5"/>
            <inertia ixx="1.7" ixy="0.0" ixz="0.0" iyy="0.156" iyz="0.0" izz="1.71"/>
        </inertial>
    </link>

    <!-- Z Stage -->
    <link name="zaxis_link">
        <visual>
            <origin xyz="0 0 0" rpy="0 0 0"/>
            <geometry>
                <mesh filename="package://testbed_description/meshes/testbed/zaxis.dae"/>
            </geometry>
        </visual>
        <collision>
            <origin xyz="0 0 0" rpy="0 0 ${PI/6}"/>
            <geometry>
                <mesh filename="package://testbed_description/meshes/testbed/zaxis_collision.dae"/>
            </geometry>
        </collision>
        <inertial>
            <mass value="7.15"/>
            <inertia ixx="0.077" ixy="0.0" ixz="0.0" iyy="0.094" iyz="0.0" izz="0.077"/>
        </inertial>
    </link>

    <!-- JOINTS -->
    <joint name="testbed_frame_to_functional_home" type="fixed">
        <origin xyz="0.4 0.4 0.2" rpy="0 0 0" />
        <parent link="testbed_frame"  />
        <child link="functional_home" />
    </joint>

    <!-- attach y_axis to frame -->
    <joint name="corexy_y" type="prismatic">
        <parent link="testbed_frame"/>
        <child link="y_axis_link"/>
        <origin xyz="${y_orig_x} ${y_orig_y} 0" rpy="0 0 0" />
        <axis xyz="0 1 0"/>
        <limit effort="100" velocity="10" lower="${y_min}" upper="${y_max}" />
        <dynamics damping="${joint_damping}" friction="${joint_friction}" />
    </joint>

    <!-- attach x_axis to y_axis -->
    <joint name="corexy_x" type="prismatic">
        <parent link="y_axis_link"/>
        <child link="x_axis_link"/>
        <origin xyz="${x_orig_x} 0 0" rpy="0 0 0" />
        <axis xyz="1 0 0"/>
        <limit effort="100" velocity="10" lower="0.0" upper="${x_max}" />
        <dynamics damping="${joint_damping}" friction="${joint_friction}" />
    </joint>

    <!-- attach yaw_axis to x_axis -->
    <joint name="yaw" type="continuous">
        <parent link="x_axis_link"/>
        <child link="yaw_axis_link"/>
        <origin xyz="0 0 0" rpy="0 0 ${yaw_orig_yaw}" />
        <axis xyz="0 0 1"/>
        <limit effort="100" velocity="10" lower="${yaw_min}" upper="${yaw_max}" />
        <dynamics damping="${joint_damping}" friction="${joint_friction}" />
    </joint>

    <!-- attach zaxis to yaw_axis -->
    <joint name="zaxis" type="prismatic">
        <parent link="yaw_axis_link"/>
        <child link="zaxis_link"/>
        <origin xyz="0 0 ${z_orig_z}" rpy="0 0 0" />
        <axis xyz="0 0 1"/>
        <limit effort="100" velocity="10" lower="${z_min}" upper="${z_max}" />
        <dynamics damping="${joint_damping}" friction="${joint_friction}" />
    </joint>

    <joint name="z_to_z_mount" type="fixed">
        <origin xyz="-0.12489 -0.031906 0.0127" rpy="0 ${-PI/2} 0" />
        <parent link="zaxis_link"  />
        <child link="z_mounting_origin" />
    </joint>
</robot>
