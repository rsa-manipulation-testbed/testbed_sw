[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit)](https://github.com/pre-commit/pre-commit)

# testbed_description

[[_TOC_]]

This package contains mesh descriptions for the APL testbed.

You can visualize the robot model:
`roslaunch testbed_description view_testbed.launch`

![rviz_sim](assets/testbed_rviz.png)

This repo only contains mesh files for geometry and collison, as well as the xacro/urdf files for testbed configurations.

If you want to change/update the testbed configuration (to add a sensor, for example) you must create a new xacro inside `/urdf/` that includes the base `testbed.xacro` as well as the `apl_tank.xacro`.

New meshes should be in DAE format and go in the `meshes` folder and should have a VERY simplified collision STL/DAE associated with them. Please remove all screws and threads to keep models small.

# CAD Models
Legacy Solidworks CAD models exist as a ZIP file, these files are outdated: [Link](https://rsa-manipulation-testbed.gitlab.io/wave-documentation/docs/cad/cad_models/)

## Onshape
We've recreated the testbed in Onshape because it's free and can export DAE files. Assemblies are separated by axis of motion and origins are in the correct locations for modeling with URDF.

Link to Onshape Model: [Link](https://cad.onshape.com/documents/a72a25a53820f75e2e7544d4/w/b3b8c6562ed982a9eb65ea40/e/870d939047b79425ab8ba9ec?renderMode=0&uiState=6712c64715dc976491f3abac)

![full_assembly](assets/testbed_onshape.png)


## Overview and TF Tree in URDF
The origin of assembly is the top plane of the tank, centered. Everything is clamped to the tank and we use that as the reference point.

### Tank
![Tank](assets/urdf_tank.png)

### Frame
![Frame](assets/urdf_frame.png)

### Y Axis
![y_axis](assets/urdf_y.png)

### X Axis
![x_axis](assets/urdf_x.png)

### Yaw Axis
![yaw_axis](assets/urdf_yaw.png)

### Z Axis
![zaxis](assets/urdf_z.png)

### TF Tree
![tree](assets/urdf_tf_tree.png)
