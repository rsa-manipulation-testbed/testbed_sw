# Testbed Motion Planning Software

This repository contains multiple ROS packages necessary for controlling and motion planning for the testbed:

  * [`testbed_config/`](testbed_config/) contains configuration files for both the hardware and Gazebo sim testbeds.  This includes Clearpath motor config and ros_control controller params.  It has minimal dependencies.
  * [`testbed_description/`](testbed_description/) contains the URDF and meshes for the testbed.
  * [`testbed_moveit_config/`](testbed_moveit_config/) contains configuration for MoveIt! for the testbed.  It **is not** dependent on Gazebo.

Gazebo support is provided by the separate [`testbed_gazebo`](https://gitlab.com/rsa-manipulation-testbed/testbed_gazebo) package to isolate Gazebo dependencies.

## Docker Support

Under `docker`, we have included a DockerFile for setting up an image running ROS Noetic with MoveIt! installed.
Please see the `README.md` under `docker` to see how to use these DockerFile if interested.

## Dependencies

Usage of this repository requires the installation of ROS Noetic, Rviz, and MoveIt! Which can be installed via the following commands:

Install ROS Noetic, RViz, and MoveIt!
```bash
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
sudo apt install curl # if you haven't already installed curl
curl -s https://raw.githubusercontent.com/ros/rosdistro/master/ros.asc | sudo apt-key add -
sudo apt update
# If you know what you're doing, you can get way with ros-noetic-desktop and installing required packages individually, see the DockerFile for list of individual packages
sudo apt install ros-noetic-desktop-full python3-catkin-tools ros-noetic-moveit
```
Note that the dependencies list was not tested, as the main development was done with Docker containers. We believe the above instructions give all packages necessary, but there may be other dependencies not reported. Please submit a pull request if you find missing dependencies.

## Installation

1. Clone the repository inside of your catkin workspace. (If you do not have a catkin workspace set up, follow the instructions from the [ROS wiki](http://wiki.ros.org/catkin/Tutorials/create_a_workspace))
2. Run `catkin build`
3. Source the setup.bash file. `source devel/setup.bash`
